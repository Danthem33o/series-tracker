package main;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.memory.StaticMemory;
import views.controllers.FoundScreenController;
import views.controllers.LoginScreenController;
import views.controllers.MainMenuController;
import views.popup.Popup;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main extends Application {

    private Stage stage;
    public static StaticMemory staticMemory;

   @Override
    public void start(Stage primaryStage) throws Exception {
       //   Init memory
       staticMemory = new StaticMemory();
       stage = primaryStage;
       launchStartScreen();
       primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    private void launchStartScreen() {
        goToLoginScreen();
        //gotToMainMenu();
    }

    /* screens */
    public void gotToMainMenu() {
        try {
            MainMenuController mainMenu = (MainMenuController) replaceSceneContent("mainScreen.fxml");
            mainMenu.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void goToFoundScreen() {
        try {
            FoundScreenController controller = (FoundScreenController) replaceSceneContent("");
            controller.setApp(this);
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void goToLoginScreen() {
        try {
            LoginScreenController controller = (LoginScreenController) replaceSceneContent("login.fxml");
           // controller.runBackground();
            stage.getScene().addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    //for (int i = 0; i < 3; i ++) {
                        System.out.println("Running...");

                    //}
                }
            });
            controller.setApp(this);
           // controller.signup();
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void goToSignUpScreen() {

    }

    /* popups */
    public void goToAdvanced() throws IOException, InterruptedException {
        new Popup(400, 600, "Advanced Search", "/views/fxml/popups/advancedPopup.fxml", this);
    }

    public void goToFind() throws IOException, InterruptedException {
        new Popup(231, 333,"Find Series", "/views/fxml/popups/findPopup.fxml", this);
    }

    public void goToDelete() throws IOException, InterruptedException {
        new Popup(400, 600, "Delete Series", "/views/fxml/popups/deletePopup.fxml", this);
    }

    public void goToAdd() throws IOException, InterruptedException {
        new Popup(400, 600, "Add Series", "/views/fxml/popups/addPopup.fxml", this);
    }


    /* Needed to change scenes */
    public Initializable replaceSceneContent(String fxml) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        InputStream in = Main.class.getResourceAsStream("/views/fxml/" + fxml);

        loader.setBuilderFactory(new JavaFXBuilderFactory());
        loader.setLocation(Main.class.getResource(fxml));

        AnchorPane page;

        page = (AnchorPane) loader.load(in);
        in.close();

        stage.setScene(new Scene(page));
        stage.sizeToScene();

        return (Initializable) loader.getController();
    }

    /* Getters & Setters */
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;

    }
}
