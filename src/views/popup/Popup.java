package views.popup;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import main.Main;

import java.io.IOException;

/**
 * Created by Daniel on 10/03/2016.
 *
 */
public class Popup {

    //Widgets
    private Stage stage;
    private Parent root;
    private Scene scene;

    //Properties
    private int height;
    private int width;
    private String title;
    private String fxml;

    private Main application;

    //Constuctors
    public Popup(int height, int width, String title, String fxml, Main main) throws IOException, InterruptedException {
        //Set the properties
        this.height = height;
        this.width = width;
        this.title = title;
        this.fxml = fxml;
        application = main;

        //Set the properties to the stage
        stage = new Stage();
        root = FXMLLoader.load(getClass().getResource(fxml));

        scene = new Scene(root, width, height);

        stage.setTitle(title);
        stage.initModality(Modality.WINDOW_MODAL);
        stage.initOwner(application.getStage().getScene().getWindow());
        stage.setScene(scene);
        stage.setAlwaysOnTop(true);
        stage.show();
    }

    public Popup(int height, int width, String title, String fxml) throws IOException {
        //Set the properties
        this.height = height;
        this.width = width;
        this.title = title;
        this.fxml = fxml;

        //Set the properties to the stage
        stage = new Stage();
        root = FXMLLoader.load(getClass().getResource(fxml));

        scene = new Scene(root, width, height);

        stage.setTitle(title);
        stage.setScene(scene);
        stage.show();
    }

    public Popup(int height, int width) {
        this.height = height;
        this.width = width;

        stage = new Stage();
    }

    public Popup() {
    }


    //stage accessors
    public void showPopup() {
        stage.show();
    }

    public void hidePopup() {
        stage.hide();
    }

    public void loadPopupFXMl(String fxml) throws IOException {
        this.fxml = fxml;
        root = FXMLLoader.load(getClass().getResource(fxml));
    }

    public void setWaitForInput(Boolean tf) {
        if (tf) stage.showAndWait();
    }

    public void setUnclick() {
        stage.initModality(Modality.WINDOW_MODAL);
        //stage.initOwner(application.getStage().getScene().getWindow());
    }

    public void setApp(Main main) {
        application = main;
    }

    public void close() {
        stage.close();
    }


    //Getters & Setters
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Parent getRoot() {
        return root;
    }

    public void setRoot(Parent root) {
        this.root = root;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        stage.setTitle(title);
    }

    public String getFxml() {
        return fxml;
    }

    public void setFxml(String fxml) {
        this.fxml = fxml;
    }

}
