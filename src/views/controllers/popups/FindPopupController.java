package views.controllers.popups;

import javafx.fxml.Initializable;
import main.Main;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Daniel on 10/03/2016.
 *
 */
public class FindPopupController implements Initializable {

    Main application;

    public void setApp(Main application) {
        this.application = application;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void close() {
        System.out.println("Close reached");

    }

    public void search() {
        System.out.println("Find reached");
    }

}
