package views.controllers;

import javafx.animation.*;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventType;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;
import main.Main;
import javafx.application.Platform;
import javafx.concurrent.*;
import javafx.event.ActionEvent;

import java.io.*;

import java.lang.*;
import javafx.event.EventHandler;


import javafx.stage.WindowEvent;
import model.BackgroundImageChanger;
import java.util.ArrayList;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Daniel on 09/06/2016.
 *
 */
public class LoginScreenController implements Initializable {

    private Main application;
    int i = new File("C:\\Users\\Daniel\\IdeaProjects\\Series 2.0\\src\\resources\\img").listFiles().length;
    boolean endBackground;

    /*  FXML Components */
    @FXML TextField     userNameTextField;
    @FXML PasswordField passPasswordField;
    @FXML Label         signUpLink;
    @FXML BorderPane    borderPane;
    @FXML ImageView     backgroundImageView;

    private Service<Void> backgroundService;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Image image = new Image("/resources/img/img1.jpg");
        System.out.println("Image loading error? " + image.isError());

        backgroundImageView.setImage(image);

        runBackground();
    }

    public void setApp(Main application) {
        this.application = application;
    }

    /*   Action Listeners   */
    public void login() {
        try {
            System.out.println("Cancelling background thread");
            backgroundService.cancel();

            countVowels("Tiger");
            countVowels("tow");
            if (backgroundService.isRunning()) backgroundService.cancel();
           /* backgroundService.setOnSucceeded(workerStateEvent -> {
                backgroundService.cancel();
            });
            application.gotToMainMenu();
            endBackground = true;
            if (true) { //    set the user and enter
                //if (userNameTextField.getText().matches())
                application.gotToMainMenu();
            } else {
                //  error
            }*/
        } catch (Exception ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //	returns the amount
    public int countVowels(String word) throws IOException {
        word = word.toLowerCase();
        Reader reader = new StringReader(word);

        String a = "a", e = "e", i = "i", o = "o", u = "u";

        ArrayList charr = new ArrayList();
        for(int x = 0; x < word.length(); x ++) {
            char c = (char) reader.read();
            charr.add(c);
        }

        int count  = 0;
        for (int x = 0; x < charr.size(); x ++) {
            if (charr.get(x).equals(e) ) {
                System.out.println("reached: " + count);
                count ++;
            }
        }

        System.out.println("Count: " + count);
        return count;
    }

    public void signup() throws InterruptedException {

    }



    public void runBackground() {
        BackgroundImageChanger backgroundImageChanger;


        backgroundService = new Service<Void>() {
            @Override
            protected Task<Void> createTask() {

                return new Task<Void>() {
                    @Override
                    protected Void call() throws Exception {
                        System.out.println(isCancelled());
                        if (isCancelled()) {
                            System.out.println(isCancelled());
                            return null ;
                        }

                        Timeline backgroundChanger = new Timeline(new KeyFrame(Duration.seconds(10), new EventHandler<ActionEvent>() {

                            @Override
                            public void handle(ActionEvent event) {
                                fadeImage(backgroundImageView);
                                System.out.println("Changing background image");
                                String backgroundStyle = String.format("/resources/img/img%d.jpg", i);
                                Image image = new Image(backgroundStyle);
                                System.out.println("Image loading error? " + image.isError());
                                backgroundImageView.setImage(image);

                                i --;
                                if (i == 0) i = new File("C:\\Users\\Daniel\\IdeaProjects\\Series 2.0\\src\\resources\\img").listFiles().length;
                            }
                        }));
                        //backgroundChanger.setCycleCount(Timeline.);
                        backgroundChanger.play();



                        return null;
                    }

                    @Override
                    public boolean cancel(boolean mayInterruptIfRunning) {
                        return super.cancel(mayInterruptIfRunning);
                    }
                };
            }
        };

        backgroundService.restart();
    }


    public void fadeImage(ImageView image) {
        FadeTransition ft = new FadeTransition(Duration.millis(2000),
                image);
        ft.setFromValue(1.0);
        ft.setToValue(0.1);
        ft.setFromValue(0.1);
        ft.setToValue(1.0);
        ft.play();
    }

}
