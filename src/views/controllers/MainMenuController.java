package views.controllers;

import javafx.fxml.Initializable;
import main.Main;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class MainMenuController implements Initializable {

    private Main application;

    public void setApp(Main application) {
        this.application = application;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }


    /* Event Handlers */
    public void add() throws IOException, InterruptedException {
        application.goToAdd();
    }

    public void delete() throws IOException, InterruptedException {
        application.goToDelete();
    }

    public void find() throws IOException, InterruptedException {
        application.goToFind();
    }

    public void advanced() throws IOException, InterruptedException {
        application.goToAdvanced();
    }
}
