package model;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Created by Daniel on 27/04/2016.
 *
 * Class that interprets and translates XML documents
 */
public class DOM {

    /**
     *
     * @param fileName  The file that holds the xml data.
     */
    public void getDocument(String fileName) {
        try {
            //   Get the file
            File inputFile = new File(fileName);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            org.w3c.dom.Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();

            //   Get all node within...
            NodeList nList = doc.getElementsByTagName("");
            for (int i = 0; i < nList.getLength(); i ++) {
                //   Get current node
                Node nNode = nList.item(i);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    //Get the main title
                    String title =
                            eElement.getElementsByTagName("title").item(0).getTextContent();

                    //get the sub title
                    String subtitle =
                            eElement.getElementsByTagName("subtitle").item(0).getTextContent();

                    //get the body
                    String body =
                            eElement.getElementsByTagName("body").item(0).getTextContent();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Problem with loading file");
        }
    }

    public static Double multiply(double a, double b) {
        return a * b;
    }

    String personName;

    public void person(String personName) {
        this.personName = personName;
    }

}

